package ru.t1.lazareva.tm.service.dto;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.lazareva.tm.api.service.dto.ISessionDtoService;
import ru.t1.lazareva.tm.dto.model.SessionDto;

@Service
@NoArgsConstructor
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDto, ISessionDtoRepository> implements ISessionDtoService {

}