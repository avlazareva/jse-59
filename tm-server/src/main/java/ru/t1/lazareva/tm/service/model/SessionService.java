package ru.t1.lazareva.tm.service.model;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.repository.model.ISessionRepository;
import ru.t1.lazareva.tm.api.service.model.ISessionService;
import ru.t1.lazareva.tm.model.Session;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

}
