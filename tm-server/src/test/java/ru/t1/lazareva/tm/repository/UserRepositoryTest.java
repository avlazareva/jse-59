package ru.t1.lazareva.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;

import static ru.t1.lazareva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest extends AbstractSchemeTest {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDtoRepository repository;

    @BeforeClass
    @Transactional
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @BeforeClass
    @SneakyThrows
    @Transactional
    public void beforeClazz() {
        repository.add(USER_1);
        repository.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    @Transactional
    public void afterClazz() {
        repository.remove(USER_1);
        repository.remove(USER_2);
    }

    @After
    @SneakyThrows
    @Transactional
    public void after() {
        for (@NotNull final UserDto user : USER_LIST) {
            repository.remove(user);
        }
        repository.clear();
    }

    @Test
    @Transactional
    public void add() {
        repository.add((USER_1));
        @Nullable final UserDto user = repository.findOneById(USER_1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_1.getId(), user.getId());
    }

    @Test
    @Transactional
    public void findAll() {
        repository.add(USER_1);
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    @Transactional
    public void existsById() throws Exception {
        @NotNull final UserDto createdUser = USER_1;
        repository.add(createdUser);
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USER_1.getId()));
    }

    @Test
    @Transactional
    public void findOneById() throws Exception {
        repository.add(USER_1);
        @Nullable final UserDto user1 = repository.findOneById(USER_1.getId());
        Assert.assertNotNull(user1);
        Assert.assertEquals(USER_1.getId(), user1.getId());
        @Nullable final UserDto user2 = repository.findOneById("");
        Assert.assertNull(user2);
    }

    @Test
    @Transactional
    public void clear() throws Exception {
        @NotNull final UserDto createdUser = USER_1;
        repository.add(createdUser);
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    @Transactional
    public void remove() throws Exception {
        @Nullable final UserDto createdUser = repository.create(USER_1.getLogin(), USER_1.getPasswordHash());
        Assert.assertNotNull(createdUser);
        repository.remove(createdUser);
        @Nullable final UserDto user = repository.findOneById(createdUser.getId());
        Assert.assertNull(user);
    }

    @Test
    @Transactional
    public void getSize() throws Exception {
        Assert.assertTrue(repository.findAll().isEmpty());
        Assert.assertEquals(0, repository.getSize());
        repository.add(USER_1);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    @Transactional
    public void findByLogin() throws Exception {
        repository.add(USER_1);
        @Nullable final UserDto user = repository.findByLogin(USER_1.getLogin());
        Assert.assertEquals(USER_1.getLogin(), user.getLogin());
    }

    @Test
    @Transactional
    public void findByEmail() throws Exception {
        repository.add(USER_1);
        @Nullable final UserDto user = repository.findByEmail(USER_1.getEmail());
        Assert.assertEquals(USER_1.getEmail(), user.getEmail());
    }

    @Test
    @Transactional
    public void isLoginExists() throws Exception {
        @Nullable final UserDto user = repository.add(USER_1);
        @Nullable final Boolean loginExists = repository.isLoginExists(user.getLogin());
        Assert.assertTrue(loginExists);
    }

    @Test
    @Transactional
    public void isEmailExists() throws Exception {
        @Nullable final UserDto user = repository.add(USER_1);
        @Nullable final Boolean emailExists = repository.isEmailExists(user.getEmail());
        Assert.assertTrue(emailExists);
    }

}
