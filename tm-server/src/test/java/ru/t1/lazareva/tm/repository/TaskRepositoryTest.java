package ru.t1.lazareva.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.lazareva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;

import static ru.t1.lazareva.tm.constant.ProjectTestData.NON_EXISTING_PROJECT_ID;
import static ru.t1.lazareva.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest extends AbstractSchemeTest {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Autowired
    private ITaskDtoRepository empty_repository;

    @NotNull
    @Autowired
    private IUserDtoRepository user_repository;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @BeforeClass
    @SneakyThrows
    @Transactional
    public void beforeClazz() {
        user_repository.add(USER_1);
        user_repository.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    @Transactional
    public void afterClazz() {
        user_repository.remove(USER_1);
        user_repository.remove(USER_2);
    }

    @After
    @SneakyThrows
    @Transactional
    public void after() {
        for (@NotNull final TaskDto task : TASK_LIST) {
            repository.remove(task);
        }
        repository.clear(USER_1.getId());
        repository.clear(USER_2.getId());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void add() {
        repository.add(USER_TASK1);
        @Nullable final TaskDto task = repository.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findAll() {
        repository.add(USER_TASK1);
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void existsById() throws Exception {
        @NotNull final TaskDto createdTask = USER_TASK1;
        empty_repository.add(createdTask);
        Assert.assertFalse(repository.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_TASK1.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void existsByIdByUserId() {
        @NotNull final TaskDto createdTask = USER_TASK1;
        empty_repository.add(createdTask);
        Assert.assertFalse(repository.existsById(USER_1.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_1.getId(), USER_TASK1.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findOneById() {
        repository.add(USER_TASK1);
        @Nullable final TaskDto task1 = repository.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(USER_TASK1.getId(), task1.getId());
        @Nullable final TaskDto task2 = repository.findOneById("");
        Assert.assertNull(task2);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findOneByIdByUserId() {
        @NotNull final TaskDto createdTask = USER_TASK1;
        repository.add(USER_1.getId(), createdTask);
        Assert.assertNull(repository.findOneById(USER_1.getId(), NON_EXISTING_TASK_ID));
        @Nullable final TaskDto task = repository.findOneById(USER_1.getId(), createdTask.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(createdTask, task);
        repository.remove(createdTask);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final TaskDto createdTask = USER_TASK1;
        repository.add(createdTask);
        repository.clear(USER_TASK1.getUserId());
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clearByUserId() {
        repository.add(USER_TASK1);
        repository.add(USER_TASK2);
        repository.clear(USER_1.getId());
        Assert.assertEquals(0, repository.getSize(USER_1.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void remove() {
        @Nullable final TaskDto createdTask = repository.add(USER_TASK1);
        Assert.assertNotNull(createdTask);
        repository.remove(createdTask);
        @Nullable final TaskDto task = repository.findOneById(USER_TASK1.getId());
        Assert.assertNull(task);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void removeByUserId() {
        @Nullable final TaskDto createdTask = repository.add(USER_TASK1);
        Assert.assertNotNull(createdTask);
        repository.remove(USER_1.getId(), createdTask);
        @Nullable final TaskDto task = repository.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNull(task);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void removeByIdByUserId() {
        @Nullable final TaskDto createdTask = repository.add(USER_TASK1);
        Assert.assertNotNull(createdTask);
        repository.removeById(USER_1.getId(), USER_TASK1.getId());
        @Nullable final TaskDto task = repository.findOneById(USER_1.getId(), USER_TASK1.getId());
        Assert.assertNull(task);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void getSize() {
        Assert.assertTrue(repository.findAll().isEmpty());
        Assert.assertEquals(0, repository.getSize());
        repository.add(USER_TASK1);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void getSizeByUserId() {
        Assert.assertTrue(repository.findAll().isEmpty());
        Assert.assertEquals(0, repository.getSize(USER_TASK1.getUserId()));
        repository.add(USER_TASK1);
        Assert.assertEquals(1, repository.getSize(USER_TASK1.getUserId()));
    }

}
