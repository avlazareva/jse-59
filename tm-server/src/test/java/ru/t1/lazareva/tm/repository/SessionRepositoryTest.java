package ru.t1.lazareva.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.lazareva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;

import java.util.Collections;

import static ru.t1.lazareva.tm.constant.ProjectTestData.NON_EXISTING_PROJECT_ID;
import static ru.t1.lazareva.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest extends AbstractSchemeTest {

    @NotNull
    @Autowired
    private ISessionDtoRepository repository;

    @NotNull
    @Autowired
    private ISessionDtoRepository empty_repository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDtoRepository user_repository;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @BeforeClass
    @SneakyThrows
    @Transactional
    public void beforeClazz() {
        user_repository.add(USER_1);
        user_repository.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    @Transactional
    public void afterClazz() {
        user_repository.remove(USER_1);
        user_repository.remove(USER_2);
    }

    @After
    @SneakyThrows
    @Transactional
    public void after() {
        for (@NotNull final SessionDto session : USER_SESSION_LIST) {
            repository.remove(session);
        }
        repository.clear(USER_1.getId());
        repository.clear(USER_2.getId());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void add() {
        repository.add((USER_1_SESSION));
        @Nullable final SessionDto session = repository.findOneById(USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findAll() {
        repository.add(USER_1_SESSION);
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findAllByUserId() throws Exception {
        repository.add(USER_1_SESSION);
        Assert.assertNotNull(repository.findAll());
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertNotNull(repository.findAll(USER_1_SESSION.getUserId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void existsById() throws Exception {
        @NotNull final SessionDto createdSession = USER_1_SESSION;
        empty_repository.add(createdSession);
        Assert.assertFalse(repository.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_1_SESSION.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void existsByIdByUserId() throws Exception {
        @NotNull final SessionDto createdSession = USER_1_SESSION;
        empty_repository.add(createdSession);
        Assert.assertFalse(repository.existsById(USER_1.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_1.getId(), createdSession.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findOneById() {
        repository.add(USER_1_SESSION);
        @Nullable final SessionDto session1 = repository.findOneById(USER_1_SESSION.getId());
        Assert.assertNotNull(session1);
        Assert.assertEquals(USER_1_SESSION.getId(), session1.getId());
        @Nullable final SessionDto session2 = repository.findOneById("");
        Assert.assertNull(session2);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findOneByIdByUserId() throws Exception {
        @NotNull final SessionDto createdSession = USER_1_SESSION;
        repository.add(USER_1.getId(), createdSession);
        Assert.assertNull(repository.findOneById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final SessionDto session = repository.findOneById(USER_1.getId(), createdSession.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(createdSession, session);
        repository.remove(createdSession);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clear() throws Exception {
        @NotNull final SessionDto createdProject = USER_1_SESSION;
        empty_repository.add(createdProject);
        empty_repository.clear(USER_1_SESSION.getUserId());
        Assert.assertEquals(0, empty_repository.getSize());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clearByUserId() throws Exception {
        empty_repository.add(USER_1_SESSION);
        empty_repository.add(USER_1_SESSION);
        empty_repository.clear(USER_1.getId());
        Assert.assertEquals(0, empty_repository.getSize(USER_1.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void remove() throws Exception {
        @Nullable final SessionDto createdSession = repository.add(USER_1_SESSION);
        Assert.assertNotNull(createdSession);
        repository.remove(createdSession);
        @Nullable final SessionDto session = repository.findOneById(USER_1_SESSION.getId());
        Assert.assertNull(session);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void removeByUserId() throws Exception {
        @Nullable final SessionDto createdSession = repository.add(USER_1_SESSION);
        Assert.assertNotNull(createdSession);
        repository.remove(USER_1.getId(), createdSession);
        @Nullable final SessionDto session = repository.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNull(session);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void removeById() throws Exception {
        @Nullable final SessionDto createdSession = repository.add(USER_1_SESSION);
        Assert.assertNotNull(createdSession);
        repository.removeById(USER_1.getId(), createdSession.getId());
        @Nullable final SessionDto session = repository.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNull(session);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void removeByIdByUserId() throws Exception {
        @Nullable final SessionDto createdSession = repository.add(USER_1_SESSION);
        Assert.assertNotNull(createdSession);
        repository.removeById(USER_1.getId(), USER_1_SESSION.getId());
        @Nullable final SessionDto session = repository.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNull(session);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void getSize() throws Exception {
        Assert.assertTrue(empty_repository.findAll().isEmpty());
        Assert.assertEquals(0, empty_repository.getSize());
        empty_repository.add(USER_1_SESSION);
        Assert.assertEquals(1, empty_repository.getSize());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void getSizeByUserId() throws Exception {
        Assert.assertTrue(empty_repository.findAll().isEmpty());
        Assert.assertEquals(0, empty_repository.getSize(USER_1_SESSION.getUserId()));
        empty_repository.add(USER_1_SESSION);
        Assert.assertEquals(1, empty_repository.getSize(USER_1_SESSION.getUserId()));
    }

}
