package ru.t1.lazareva.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.lazareva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;

import static ru.t1.lazareva.tm.constant.ProjectTestData.*;

@Repository
@Scope("prototype")
@Category(UnitCategory.class)
public final class ProjectRepositoryTest extends AbstractSchemeTest {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Autowired
    private IUserDtoRepository user_repository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    @SneakyThrows
    @Transactional
    public void before() {
        user_repository.add(USER_1);
        user_repository.add(USER_2);
    }

    @After
    @SneakyThrows
    @Transactional
    public void after() {
        for (@NotNull final ProjectDto project : PROJECT_LIST) {
            repository.remove(project);
        }
        repository.clear(USER_1.getId());
        repository.clear(USER_2.getId());
        user_repository.remove(USER_1);
        user_repository.remove(USER_2);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void add() {
        repository.add(USER_PROJECT1);
        @Nullable final ProjectDto project = repository.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findAll() throws Exception {
        repository.add(USER_PROJECT1);
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void existsById() throws Exception {
        @NotNull final ProjectDto createdProject = USER_PROJECT1;
        repository.add(createdProject);
        Assert.assertFalse(repository.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_PROJECT1.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void existsByIdByUserId() throws Exception {
        @NotNull final ProjectDto createdProject = USER_PROJECT1;
        repository.add(createdProject);
        Assert.assertFalse(repository.existsById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findOneById() {
        repository.add(USER_PROJECT1);
        @Nullable final ProjectDto project1 = repository.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(USER_PROJECT1.getId(), project1.getId());
        @Nullable final ProjectDto project2 = repository.findOneById("");
        Assert.assertNull(project2);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void findOneByIdByUserId() throws Exception {
        @NotNull final ProjectDto createdProject = USER_PROJECT1;
        repository.add(USER_1.getId(), createdProject);
        Assert.assertNull(repository.findOneById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDto project = repository.findOneById(USER_1.getId(), createdProject.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(createdProject, project);
        repository.remove(createdProject);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clear() throws Exception {
        @NotNull final ProjectDto createdProject = USER_PROJECT1;
        repository.add(createdProject);
        repository.clear(USER_PROJECT1.getUserId());
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clearByUserId() throws Exception {
        repository.add(USER_PROJECT1);
        repository.add(USER_PROJECT2);
        repository.clear(USER_1.getId());
        Assert.assertEquals(0, repository.getSize(USER_1.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void remove() throws Exception {
        @Nullable final ProjectDto createdProject = repository.add(USER_PROJECT1);
        Assert.assertNotNull(createdProject);
        repository.remove(createdProject);
        @Nullable final ProjectDto project = repository.findOneById(USER_PROJECT1.getId());
        Assert.assertNull(project);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void removeByUserId() {
        @Nullable final ProjectDto createdProject = repository.add(USER_PROJECT1);
        Assert.assertNotNull(createdProject);
        repository.remove(USER_1.getId(), createdProject);
        @Nullable final ProjectDto project = repository.findOneById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNull(project);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void removeByIdByUserId() {
        @Nullable final ProjectDto createdProject = repository.add(USER_PROJECT1);
        Assert.assertNotNull(createdProject);
        repository.removeById(USER_1.getId(), USER_PROJECT1.getId());
        @Nullable final ProjectDto project = repository.findOneById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNull(project);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void getSize() {
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
        Assert.assertEquals(0, repository.getSize());
        repository.add(USER_PROJECT1);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void getSizeByUserId() {
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
        Assert.assertEquals(0, repository.getSize(USER_PROJECT1.getUserId()));
        repository.add(USER_PROJECT1);
        Assert.assertEquals(1, repository.getSize(USER_PROJECT1.getUserId()));
    }

}
